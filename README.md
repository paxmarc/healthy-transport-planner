# Healthy Transport Planner

This readme covers the developer-side setup for the application. For a user manual, please refer to the wiki.

# Backend

## Dependencies to Install
* [Docker](https://www.docker.com/products/docker)
* [Maven](https://maven.apache.org/)
* [Postgresql](https://www.postgresql.org/download/) (for running psql)

## Using Postgres 9.4 via Docker
`make setup`

## Running postgres 9.4
You can run the postgres container using:
`make docker`

Note: The username and password will both be set to root, and the database name will be set to TransportPlanner

## Running SQL commands on the running postgres container
You can now login to your postgres server with
`make sql`

Then type in the password ("root").

## Running the backend web server
`make mvn`

## Cleaning up the Docker postgres container
`make clean`

# Frontend

## Dependencies to Install
* [Ionic](http://ionicframework.com/docs/guide/installation.html)
* [npm](https://www.npmjs.com/)
* `npm install`

## Run webpack
`make watch`

## Serve the webpage
`make serve`

## Serving it on an iOS emulator

### TL;DR (for OSX):

1. Install npm
2. `sudo npm install -g cordova`
3. `sudo npm install -g ionic`
4. `ionic resources --icon`
5. `cordova platform update ios`
6. `ionic build ios`
7. `ionic emulate ios`

If the Android sdk is installed as well, you can run `ionic build` and `ionic emulate` for the `android` target

### Troubleshooting
http://stackoverflow.com/questions/38615582/error-building-ios-with-cordova