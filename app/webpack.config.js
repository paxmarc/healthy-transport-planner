var webpack = require('webpack'),
    path = require('path');

module.exports = {
    entry: ["./js-webpack/app.module.js"],

    output: {
        path: "./www/js",
        filename: "app.js"
    },

    resolve: {
      root: [
        path.resolve('./js-webpack')
      ]
    },

    module: {
      loaders: [
        // CSS File loading
        {
          test: /\.css$/,
          loader: 'style!css'
        },
        // ES6 transpiling
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        // Load HTML to string
        {
          test: /\.html$/,
          loader: 'html?attrs=false'
        },
        // Load JSON objects from file
        {
          test: /\.json$/,
          loader: 'json'
        }
      ]
    }
};
