'use strict';

config.$inject = ['$stateProvider'];

export default function config($stateProvider) {
  $stateProvider
    .state('healthyTripPlanner.tripChoices', {
      url: '/add-trip-2',
      params: {'origin':null, 'dest': null},
      views: {
        'side-menu21': {
          template: require('./tripChoices.html'),
          controller: 'tripChoicesCtrl',
          controllerAs: 'tcvm',
          resolve: {
            tripDetails: ['TripDetails', '$stateParams', function(TripDetails, $stateParams) {
              return TripDetails.sendRequest($stateParams.origin, $stateParams.dest);
            }]
          }
        }
      }
    })
}
