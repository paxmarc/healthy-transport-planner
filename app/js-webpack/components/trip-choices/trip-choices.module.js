'use strict';

import config from './config';
import TripChoicesCtrl from './trip-choices.ctrl';

export default angular.module('components.tripChoices', [

]).controller('tripChoicesCtrl', TripChoicesCtrl)
  .config(config)
  .name;
