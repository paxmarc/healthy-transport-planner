'use strict';

export default class TripChoicesCtrl {
  static $inject = ['$scope', '$state', '$stateParams', 'tripDetails'];

  constructor($scope, $state, $stateParams, tripDetails) {
    this.$stateParams = $stateParams;
    this.$scope = $scope;
    this.$state = $state;

    this.origin = this.$stateParams.origin;
    this.dest = this.$stateParams.dest;

    this.$scope.allRoutes = tripDetails.data.allRoutes;
  }

  transition(route) {
    this.$state.go('healthyTripPlanner.tripDetails', {route: route});
  }
}
