'use strict';

import login from './login/login.module';
import register from './register/register.module';
import menu from './menu/menu.module';
import savedTrips from './saved-trips/saved-trips.module';
import badges from './badges/badges.module';
import profile from './profile/profile.module';
import tripDetails from './trip-details/trip-details.module';
import addTrip from './add-trip/add-trip.module';
import tripChoices from './trip-choices/trip-choices.module';

/*
  Bootstrapping function for components.
*/

export default angular.module('components', [
  login,
  register,
  savedTrips,
  badges,
  profile,
  tripDetails,
  addTrip,
  tripChoices,
  menu
]).name;
