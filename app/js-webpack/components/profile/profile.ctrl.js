'use strict';

export default class ProfileCtrl {
  static $inject = ['Users', 'summaries'];

  constructor(Users, summaries) {
    this.Users = Users;
    this.summaries = summaries;

    this.labels = ["Walking (km)", "Bus (km)", "Train (km)"];
    this.weekly = summaries.data.weekly;

    this.total = summaries.data.total;

  }
}
