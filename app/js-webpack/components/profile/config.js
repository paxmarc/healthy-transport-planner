'use strict';

config.$inject = ['$stateProvider'];

export default function config($stateProvider) {
  $stateProvider
    .state('healthyTripPlanner.profile', {
      url: '/profile',
      cache: false,
      views: {
        'side-menu21': {
          template: require('./profile.html'),
          controller: 'profileCtrl',
          controllerAs: 'profile',
          resolve: {
            summaries: ['Summaries', function(Summaries) {
              return Summaries.getSummaries();
            }]
          }
        }
      }
    })
}
