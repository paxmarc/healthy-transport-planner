'use strict';

import config from './config';
import ProfileCtrl from './profile.ctrl';

export default angular.module('components.profile', [

]).controller('profileCtrl', ProfileCtrl)
  .config(config)
  .name;
