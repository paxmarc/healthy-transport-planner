'use strict';

config.$inject = ['$stateProvider'];

export default function config($stateProvider) {
  $stateProvider
    .state('healthyTripPlanner.tripDetails', {
      url: '/trip-details',
      params: {'route': null},
      views: {
        'side-menu21': {
          template: require('./tripDetails.html'),
          controller: 'tripDetailsCtrl',
          controllerAs: 'tdvm'
        }
      }
    })
}
