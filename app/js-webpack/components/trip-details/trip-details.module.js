'use strict';

import config from './config';
import TripDetailsCtrl from './trip-details.ctrl.js';

export default angular.module('components.trip-details', [
]).controller('tripDetailsCtrl', TripDetailsCtrl)
  .config(config)
  .name;
