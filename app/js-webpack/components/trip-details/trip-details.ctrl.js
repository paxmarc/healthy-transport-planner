'use strict';

export default class TripDetailsCtrl {
  static $inject = ['$http', '$state', '$stateParams', 'Users'];

  constructor($http, $state, $stateParams, Users) {
    this.$http = $http;
    this.$state = $state;
    this.Users = Users;

    this.route = $stateParams.route;

    this.error = "";

    this.newTrip = {
      userId: -1,
      walkingDistance: "",
      trainDistance: "",
      busDistance: ""
    }
  }

  saveDistance() {
    this.newTrip.userId = parseInt(this.Users.getUserId());
    this.newTrip.walkingDistance = this.route.walkingDistance;
    this.newTrip.trainDistance = this.route.trainDistance;
    this.newTrip.busDistance = this.route.busDistance;

    var request = this.$http({
      method: 'POST',
      url: 'http://localhost:8080/trip/new',
      data: this.newTrip
    }).then(response => {
      this.$state.go('healthyTripPlanner.savedTrips');
    }, data => {
      this.error = "Error saving trip distance data.";
    });
  }
}
