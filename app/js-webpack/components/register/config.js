'use strict';

config.$inject = ['$stateProvider']

export default function config($stateProvider) {
  $stateProvider
  .state('healthyTripPlanner.register', {
      url: '/register',
      cache: false,
      views: {
        'side-menu21': {
          template: require('./register.html'),
          controller: 'registerCtrl',
          controllerAs: 'vm'
      }
    }
  })
}
