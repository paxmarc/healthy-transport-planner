'use strict';
import config from './config';
import RegisterCtrl from './register.ctrl';

export default angular.module('components.register', [

]).controller('registerCtrl', RegisterCtrl)
  .config(config)
  .name;
