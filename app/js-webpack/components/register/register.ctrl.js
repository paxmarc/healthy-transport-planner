'use strict';

export default class RegisterCtrl {
  static $inject = ['$state', '$auth']

  constructor($state, $auth) {
    this.$state = $state;
    this.$auth = $auth;

    this.newUser = {
      name: "",
      email: "",
      password: "",
    }

    this.error = "";
  }

  register() {
    this.error = "";
    this.$auth.signup(this.newUser).then(response => {
      this.$auth.setToken(response);
      this.$state.go('healthyTripPlanner.savedTrips');
    }, response => {
      if (response.status === 401) {
        this.error = "A user with this email already exists!";
      } else {
        this.error = "There was an error registering your account. Please try again later."
      }
    });
  }
}
