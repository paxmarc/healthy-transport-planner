"use strict";
import config from './config';
import LoginCtrl from './login.ctrl.js';

export default angular.module('components.login', [

]).controller('loginCtrl', LoginCtrl)
  .config(config)
  .name;
