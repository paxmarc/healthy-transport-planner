'use strict';

export default class LoginCtrl {
  static $inject = ['$state', '$auth', '$ionicPopup']

  constructor($state, $auth, $ionicPopup) {
    this.$state = $state;
    this.$auth = $auth;
    this.$ionicPopup = $ionicPopup;

    this.user = {
      email: "",
      password: ""
    }

    this.error = ""

    if (this.$auth.isAuthenticated()) {
      this.$state.go('healthyTripPlanner.savedTrips');
    }
  }

  login() {
    this.error = ""

    this.user.email = this.user.email.trim()
    this.user.password = this.user.password.trim()

    if (this.user.email.length === 0 || this.user.password.length === 0) {
      this.error = "One or more missing fields."
      return;
    }

    this.$auth.login(this.user).then(response => {
        this.$auth.setToken(response);
        this.$ionicPopup.alert({
            title: 'Success',
            content: 'You have successfully logged in!'
          })
        this.$state.go('healthyTripPlanner.savedTrips');
    }, data => {
        if (data.status === 401) {
          this.error = "Invalid username or password.";
        } else {
          this.error = "There was error logging you in. Please try again later."
        }
    });
  }
}
