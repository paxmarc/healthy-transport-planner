config.$inject = ['$stateProvider', '$urlRouterProvider'];

export default function config($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
        url: '/login',
        cache: false,
        template: require('./login.html'),
        controller: 'loginCtrl',
        controllerAs: 'vm'
      });

  // set fallback url for start page.
  $urlRouterProvider.otherwise('/login');
}
