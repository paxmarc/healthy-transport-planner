'use strict';

import config from './config';
import SavedTripsCtrl from './saved-trips.ctrl';

export default angular.module('components.saved-trips', [

]).controller('savedTripsCtrl', SavedTripsCtrl)
  .config(config)
  .name;
