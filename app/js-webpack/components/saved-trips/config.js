'use strict';

config.$inject = ['$stateProvider']

export default function config($stateProvider) {
  $stateProvider
    .state('healthyTripPlanner.savedTrips', {
      url: '/trips',
      cache: false,
      views: {
        'side-menu21': {
          template: require('./trips.html'),
          controller: 'savedTripsCtrl',
          controllerAs: 'stvm',
          resolve: {
            savedTrip: ['SavedTrips', function(SavedTrips) {
              return SavedTrips.getSavedTrips();
            }]
          }
        }
      }
    })
}
