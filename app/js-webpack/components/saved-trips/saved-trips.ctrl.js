'use strict';

export default class SavedTripsCtrl {
  static $inject = ['$state', '$http', 'savedTrip'];

  constructor($state, $http, savedTrip) {
    this.$state = $state;
    this.$http = $http;
    this.trips = [];

    if (savedTrip.status === true) {
      this.trips = savedTrip.data['savedtrip'];
    } else {
      this.error = "Sorry, we could not reach the server. You can still add a new trip"
    }
  }

  deleteTrip(trip) {
    this.trips.splice(this.trips.indexOf(trip), 1);
    this.$http.delete("http://localhost:8080/savedtrip/{0}".replace('{0}',trip.id));
  };

  transitionToTripChoices(trip) {
    this.$state.go('healthyTripPlanner.tripChoices', {origin: trip.origin, dest: trip.dest});
  }
}
