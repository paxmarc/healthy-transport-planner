'use strict';

config.$inject = ['$stateProvider'];

export default function config($stateProvider) {
  $stateProvider
    .state('healthyTripPlanner.addTrip', {
      url: '/add-trip-1',
      views: {
        'side-menu21': {
          template: require('./addTrip.html'),
          controller: 'addTripCtrl',
          controllerAs: 'atvm'
        }
      }
    })
}
