'use strict';

import config from './config';
import AddTripCtrl from './add-trip.ctrl';

export default angular.module('components.addTrip', [

]).controller('addTripCtrl', AddTripCtrl)
  .config(config)
  .name;
