'use strict';

export default class AddTripCtrl {
  static $inject = ['$state', '$http'];

  constructor($state, $http) {
    this.$state = $state;
    this.$http = $http;

    this.error = {};
    this.trip = {};
  }

  submit(formCtrl) {
    if (formCtrl.$valid) {
      this.transition();
      return;
    }

    this.onError(formCtrl);
  }

  transition() {
    if (this.trip.save) {
      this.saveTrip(this.trip.origin, this.trip.dest);
    } else {
      this.$state.go('healthyTripPlanner.tripChoices', this.trip);
    }
  }

  saveTrip() {
    this.$http({
      method: 'POST',
      url: 'http://localhost:8080/savedtrip',
      data: this.trip
    }).then(() => {
      this.$state.go('healthyTripPlanner.tripChoices', this.trip);
    }, data => {
      this.error.server = "Sorry, we had difficulties saving your trip. Please try again.";
    });
  }

  onError(formCtrl) {
    if (!formCtrl.origin.$valid) {
      this.error.origin = "Please select a valid from address";
    }

    if (!formCtrl.dest.$valid) {
      this.error.dest = "Please select a valid to address";
    }
  }
}
