'use strict';

import config from './config';
import BadgesCtrl from './badges.ctrl';

export default angular.module('components.badges', [

]).controller('badgesCtrl', BadgesCtrl)
  .config(config)
  .name;
