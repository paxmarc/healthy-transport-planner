'use strict';

config.$inject = ['$stateProvider'];

export default function config($stateProvider) {
  $stateProvider
    .state('healthyTripPlanner.badges', {
      url: '/badges',
      cache: false,
      views: {
        'side-menu21': {
          template: require('./badges.html'),
          controller: 'badgesCtrl',
          controllerAs: 'bdvm',
          resolve: {
            allBadgesFn: ['Badges', function(Badges) {
              return Badges.getAllBadges();
            }],
            userBadgesFn: ['Badges', function(Badges) {
              return Badges.getUserBadges();
            }],
            earnedDatesFn: ['Badges', function(Badges) {
              return Badges.getEarnedDates();
            }]
          }
        }
      }
    })
}
