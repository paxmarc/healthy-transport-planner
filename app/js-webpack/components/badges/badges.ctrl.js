'use strict';

export default class BadgesCtrl {
  static $inject = ['Badges', 'Users', 'userBadgesFn', 'allBadgesFn', 'earnedDatesFn'];

  /**
   * Initialize all the necessary global variables.
   * @param Badges
   * @param Users
   * @param userBadgesFn
   * @param allBadgesFn
   * @param earnedDatesFn
   */
  constructor(Badges, Users, userBadgesFn, allBadgesFn, earnedDatesFn) {
    this.Badges = Badges;
    this.Users = Users;
    this.userBadges = userBadgesFn.data.badges;
    this.allBadges = allBadgesFn.data.badges;
    this.earnedDates = earnedDatesFn.data.earnedDates;

    this.missingBadges = this.missingBadgesFn();

    this.colourClasses = {0: "bronze", 1: "silver", 2: "gold"};
  }

  /**
   * Returns all the badges that the user has not earned yet.
   * @returns {Array}
   */
  missingBadgesFn() {
    var missingBadges = [];

    this.allBadges.forEach(function(badge) {
      var earned = false;
      for (var index in this.userBadges) {
        if (this.userBadges[index].id == badge.id) {
          earned = true;
          break;
        }
      }

      if (!earned) {
        missingBadges.push(badge);
      }
    }, this);

    return missingBadges;
  }

  /**
   * Returns the icon colour for this badge (bronze, silver, or gold), based on the badge ID.
   * @param badgeId - The ID number of the badge
   * @returns {colourClass String}
   */
  getColourClass(badgeId) {
    var mod = (badgeId - 1) % 3;
    return this.colourClasses[mod];
  }

  /**
   * Returns the Date that this badge was earned on, by the user.
   * @param badgeId - The ID number of the badge
   * @returns {Date String}
   */
  getEarnedDate(badgeId) {
    for (var index in this.earnedDates) {
      var userBadge = this.earnedDates[index];
      if (userBadge.badge.id == badgeId) {
        return userBadge.earnedDate;
      }
    }
  }
}
