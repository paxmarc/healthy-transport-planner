export default class MenuCtrl {
  static $inject = ['$state', '$auth', 'Users'];

  constructor($state, $auth, Users) {
    this.$state = $state;
    this.$auth = $auth;
    this.Users = Users;
  }

  logout() {
    this.$auth.logout();
    this.$state.go('login');
  }

  isLoggedIn() {
    return this.$auth.isAuthenticated();
  }
}
