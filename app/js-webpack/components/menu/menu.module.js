'use strict';
import config from './config';
import MenuCtrl from './menu.ctrl.js';

export default angular.module('components.menu', [

]).controller('menuCtrl', MenuCtrl)
  .config(config)
  .name;
