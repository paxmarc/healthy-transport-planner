'use strict';

config.$inject = ['$stateProvider']

// default state config to import the menu bar

export default function config($stateProvider) {
  $stateProvider
  .state('healthyTripPlanner', {
        controllerAs: "vm",
        url: '/menu',
        template: require('./menu.html'),
        controller: 'menuCtrl'
  })
}
