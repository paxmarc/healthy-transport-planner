'use strict';

class SavedTrips {
  static $inject = ['$http'];

  constructor($http) {
    this.$http = $http;
  }

  getSavedTrips() {
    return this.$http.get('http://localhost:8080/savedtrip').then(response => {
        return {status:true, data: response.data}
    }, response => {
        return {status:false, data: response.data}
    });
  }
}

export default angular.module('services.saved-trips', [])
  .service('SavedTrips', SavedTrips)
  .name;
