'use strict';

class Badges {
  static $inject = ['$http'];

  constructor($http) {
    this.$http = $http;
  }

  getUserBadges() {
    return this.$http.get("http://localhost:8080/badge/user");
  }

  getAllBadges() {
    return this.$http.get("http://localhost:8080/badge");
  }

  getEarnedDates() {
    return this.$http.get("http://localhost:8080/badge/user/earned");
  }
}

export default angular.module('services.badges', [])
  .service('Badges', Badges)
  .name;
