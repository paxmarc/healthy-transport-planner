'use strict';

import users from 'services/users.service';
import badges from 'services/badges.service';
import summaries from 'services/summary.service';
import tripDetails from 'services/tripDetails.service';
import savedTrip from 'services/saved-trips.service';

/*
  Bootstrapping function for services module.
  Imports all the used services and bundles them into one module.
*/

export default angular.module('services', [
  users,
  badges,
  summaries,
  tripDetails,
  savedTrip
]).name;
