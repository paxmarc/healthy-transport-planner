'use strict';
/*
Service for accessing user information present in the JSON web token.
*/
class Users {
  static $inject = ['$auth'];

  constructor($auth) {
    this.$auth = $auth;
  }

  // Retrieve user's full name from the JWT.
  getUserName() {
    if (this.$auth.isAuthenticated()) {
      return this.$auth.getPayload()['name'];
    }
  }

  // Fetch the join date from the JWT as a timestamp.
  getJoinDate() {
    if (this.$auth.isAuthenticated()) {
      return this.$auth.getPayload()['joined'];
    }
  }

  // Fetch the user's user ID. (may not be used?)
  getUserId() {
    if (this.$auth.isAuthenticated()) {
      return this.$auth.getPayload()['sub'];
    }
  }

}

export default angular.module('services.users', [])
  .service('Users', Users)
  .name;
