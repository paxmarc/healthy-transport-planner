'use strict';

/*
Service for accessing statistics summaries about trips.
*/
class Summaries {
  static $inject = ['$http'];

  constructor($http) {
    this.$http = $http;
  }

  /*
    Retrieves the summaries from the server.
    Returns both a weekly and a total summary, under 'weekly'
    and 'total' attributes respectively.
  */
  getSummaries() {
      return this.$http.get("http://localhost:8080/stats");
  }
}

export default angular.module('services.summaries', [])
  .service('Summaries', Summaries)
  .name;
