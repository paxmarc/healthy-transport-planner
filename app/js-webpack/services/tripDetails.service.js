'use strict';

class TripDetails {
  static $inject = ['$http'];

  constructor($http) {
    this.$http = $http;
  }

  sendRequest(origin, dest) {
    return this.$http({
      method: 'GET',
      url: 'http://localhost:8080/trip-details/',
      params: {origin: origin, dest: dest}
    });
  }

}

export default angular.module('services.tripDetails', [])
  .service('TripDetails', TripDetails)
  .name;
