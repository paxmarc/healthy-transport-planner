"use strict";

import services from 'services/services.module';
import components from 'components/components.module';
import satellizer from 'satellizer';
import 'chart.js';
import chart from 'angular-chart.js'
import config from 'app.config';

// Bootstrap components
angular.module('app', [ 'ionic',
                        'ngAutocomplete',
                        satellizer,
                        chart,
                        services,
                        components])
.config(config)
.run(ionicLaunch)

function ionicLaunch($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
}
