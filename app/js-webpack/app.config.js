'use strict';
config.$inject = ['$locationProvider', '$authProvider'];

export default function config($locationProvider, $authProvider) {
  // eliminate # character from urls
  $locationProvider.html5Mode(true);

  // set satellizer default option
  $authProvider.baseUrl = "http://localhost:8080"

}
