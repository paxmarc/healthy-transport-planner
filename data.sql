-- Walk
INSERT INTO badge (id, name, description) VALUES (1, 'Downtown Stroll', 'Travel 500m by walking.');
INSERT INTO badge (id, name, description) VALUES (2, 'Afternoon Hike', 'Travel 5km by walking.');
INSERT INTO badge (id, name, description) VALUES (3, 'Marathon Walk', 'Travel 50km by walking.');
-- Bus
INSERT INTO badge (id, name, description) VALUES (4, 'Downtown Bus', 'Travel 500m by bus.');
INSERT INTO badge (id, name, description) VALUES (5, 'Suburban Bus', 'Travel 5km by bus.');
INSERT INTO badge (id, name, description) VALUES (6, 'Inter-city Bus', 'Travel 50km by bus.');
-- Train
INSERT INTO badge (id, name, description) VALUES (7, 'Downtown Train', 'Travel 500m by train.');
INSERT INTO badge (id, name, description) VALUES (8, 'Suburban Train', 'Travel 5km by train.');
INSERT INTO badge (id, name, description) VALUES (9, 'Intercity Train', 'Travel 50km by train.');
-- Membership
INSERT INTO badge (id, name, description) VALUES (10, 'Bronze Member', 'Be a member for 1 month.');
INSERT INTO badge (id, name, description) VALUES (11, 'Silver Member', 'Be a member for 6 months.');
INSERT INTO badge (id, name, description) VALUES (12, 'Gold Member', 'Be a member for 1 year.');
-- Total Trips
INSERT INTO badge (id, name, description) VALUES (13, 'Curious Adventurer', 'Complete 10 trips.');
INSERT INTO badge (id, name, description) VALUES (14, 'Seasoned Traveller', 'Complete 100 trips.');
INSERT INTO badge (id, name, description) VALUES (15, 'Grand Explorer', 'Complete 1000 trips.');
-- Week Trips
INSERT INTO badge (id, name, description) VALUES (16, 'Occasional Adventurer', 'Complete 5 trips in a week.');
INSERT INTO badge (id, name, description) VALUES (17, 'Regular Traveller', 'Complete 10 trips in a week.');
INSERT INTO badge (id, name, description) VALUES (18, 'Enthusiastic Explorer', 'Complete 15 trips in a week.');

-- Extra SQL statements after signing up a user.
-- INSERT INTO user_badge (user_badge_id, earned_date, user_id, badge_id) VALUES (1, '2016-10-01'::date, 10, 1); 
-- INSERT INTO user_badge (user_badge_id, earned_date, user_id, badge_id) VALUES (2, '2016-10-03'::date, 10, 2); 
-- INSERT INTO user_badge (user_badge_id, earned_date, user_id, badge_id) VALUES (3, '2016-10-20'::date, 10, 4); 
-- INSERT INTO user_badge (user_badge_id, earned_date, user_id, badge_id) VALUES (4, '2016-10-03'::date, 10, 7); 
-- INSERT INTO user_badge (user_badge_id, earned_date, user_id, badge_id) VALUES (5, '2016-10-03'::date, 10, 8); 
-- INSERT INTO user_badge (user_badge_id, earned_date, user_id, badge_id) VALUES (6, '2016-10-20'::date, 10, 13); 
