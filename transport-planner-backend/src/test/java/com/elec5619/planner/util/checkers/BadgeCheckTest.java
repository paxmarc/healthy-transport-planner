package com.elec5619.planner.util.checkers;

import com.elec5619.planner.domain.dao.BadgeRepository;
import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.Badge;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.service.JsonWebTokenService;
import com.elec5619.planner.service.SummaryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

/**
 * Created by eliemoreau on 24/10/2016.
 */

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class BadgeCheckTest {

    @Autowired
    TestEntityManager entityManager;

    @Mock
    UserRepository userRepository;

    @Mock
    BadgeRepository badgeRepository;

    @Mock
    SummaryService summaryService;

    @InjectMocks
    BadgeChecker badgeChecker;

    User u;
    Badge b1;
    Badge b2;

    @Before
    public void setUp() {
        badgeChecker = new BadgeChecker();
        MockitoAnnotations.initMocks(this);

        this.u = new User("m@m.m", "password", "m");
        entityManager.persist(this.u);
        this.b1 = new Badge("Bronze Member", "Be a member for 1 month.");
        entityManager.persist(this.b1);
        this.b2 = new Badge("Silver Member", "Be a member for 6 months.");
        entityManager.persist(this.b2);

        // Inject a result to be returned when userRepository.findOne() is called.
        when(userRepository.findOne(any())).thenReturn(this.u);

        // Inject a result to be returned when badgeRepository.findAll() is called.
        when(badgeRepository.findAll()).thenReturn(Arrays.asList(this.b1, this.b2));
    }

    @Test
    public void testCheckNewBadgesWithOldBadges() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -2);
        u.setJoinDate(c.getTime());

        // User has earned Badge 1 before, but not yet qualified for Badge 2.
        Set<Long> currentBadges = new HashSet<>();
        currentBadges.add(this.b1.getId());
        List<Badge> newBadges = badgeChecker.checkNewBadges(this.u.getId(), currentBadges);

        // Assert that there are no new badges for the user.
        assertEquals(Arrays.asList(), newBadges);
    }

    @Test
    public void testCheckNewBadgesWithNewBadges() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -2);
        u.setJoinDate(c.getTime());

        // User has not earned any badges before, but they are now qualified for Badge 1.
        List<Badge> newBadges = badgeChecker.checkNewBadges(this.u.getId(), new HashSet<>());

        // Assert that the user has earned 1 new badge.
        assertEquals(Arrays.asList(this.b1), newBadges);
    }

    @Test
    public void testCheckNewBadgesWithOldAndNewBadges() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -6);
        u.setJoinDate(c.getTime());

        // User has earned Badge 1 before, and they are qualified for Badge 2.
        Set<Long> currentBadges = new HashSet<>();
        currentBadges.add(this.b1.getId());
        List<Badge> newBadges = badgeChecker.checkNewBadges(this.u.getId(), currentBadges);

        // Assert that the user has earned 1 new badge.
        assertEquals(Arrays.asList(this.b2), newBadges);
    }

    @Test
    public void testCheckBadgeWithNewBadge() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -2);
        u.setJoinDate(c.getTime());

        // User has been a member long enough to earn Badge 1.
        boolean gainNewBadge = badgeChecker.checkBadge(this.u.getId(), this.b1);

        // Assert that User has earned Badge 1.
        assertTrue(gainNewBadge);
    }

    @Test
    public void testCheckBadgeWithNoNewBadge() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -10);
        u.setJoinDate(c.getTime());

        // User has not been a member long enough to earn Badge 1.
        boolean gainNewBadge = badgeChecker.checkBadge(this.u.getId(), this.b1);

        // Assert that User has not earned Badge 1.
        assertFalse(gainNewBadge);
    }

}
