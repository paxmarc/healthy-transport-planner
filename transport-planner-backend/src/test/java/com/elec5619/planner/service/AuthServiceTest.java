package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.util.wrappers.LoginRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import static org.mockito.Mockito.*;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.ArrayList;

/**
 * Created by marcuspaxton on 21/10/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private MockEnvironment mockEnvironment;

    @Mock
    private JsonWebTokenService jsonWebTokenService;

    @InjectMocks
    private AuthService authService = new AuthService();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPasswordHashing() {
        User testUser = new User("m@m.m", "password", "Test User");
        testUser = authService.hashUserPassword(testUser);

        Assert.assertNotEquals("password", testUser.getPassword());
        Assert.assertTrue(authService.verifyPassword("password", testUser.getPassword()));
        Assert.assertFalse(authService.verifyPassword("notPassword", testUser.getPassword()));
    }

    @Test
    public void testWrongUsernameLogin() {
        // expected outputs
        ArrayList<User> empty = new ArrayList<>();
        LoginRequest lr = new LoginRequest("email", "pass");

        // expect repository to find nothing.
        when(userRepository.findByEmail(anyString())).thenReturn(empty);

        Assert.assertNull(authService.loginUser(lr));
    }

    @Test
    public void testWrongPasswordLogin() {
        LoginRequest lr = new LoginRequest("m@m.m", "notPassword");
        User newUser = new User("m@m.m", BCrypt.hashpw("password", BCrypt.gensalt()), "Test User");
        ArrayList<User> hasOne = new ArrayList<>();
        hasOne.add(newUser);

        // expect repository to find the one user.
        when(userRepository.findByEmail(anyString())).thenReturn(hasOne);

        Assert.assertNull(authService.loginUser(lr));
    }

    @Test
    public void testCorrectLogin() {
        LoginRequest lr = new LoginRequest("m@m.m", "password");
        User newUser = new User("m@m.m", BCrypt.hashpw("password", BCrypt.gensalt()), "Test User");
        newUser.setId(1L);
        ArrayList<User> hasOne = new ArrayList<>();
        hasOne.add(newUser);

        // expect repository to find the one user.
        when(userRepository.findByEmail(anyString())).thenReturn(hasOne);
        // mock environment return
        when(jsonWebTokenService.generateJwt(any())).thenReturn("lol");

        Assert.assertNotNull(authService.loginUser(lr));
    }


}
