package com.elec5619.planner.service;

import com.elec5619.planner.domain.model.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by Rolo on 23/10/2016.
 */

public class SavedTripServiceImplTest {

    private SavedTripService savedTripService;

    @Before
    public void setUp() throws Exception {
        savedTripService = new SavedTripServiceImpl();
    }

    @Test
    public void testGetSavedTripsReturnsAnEmptyListWhenUserIsNull() {
        assertEquals(0, savedTripService.getSavedTrips((User) null).size());
    }

    @Test
    public void testSaveTripWhenUserIsNull() {
        assertFalse(savedTripService.saveTrip((User) null, "abc", "123"));
    }

}