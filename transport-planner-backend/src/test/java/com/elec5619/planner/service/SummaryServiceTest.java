package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.TripRepository;
import com.elec5619.planner.domain.model.Trip;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcuspaxton on 24/10/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class SummaryServiceTest {

    @Mock
    private TripRepository tripRepository;

    @InjectMocks
    private SummaryService summaryService = new SummaryService();

    @Test
    public void testAdditionWorks() {
        Trip t1 = new Trip(1L, 1.0, 1.0, 1.0);
        Trip t2 = new Trip(1L, 3.0, 6.0, 4.0);

        List<Trip> l = new ArrayList<>();
        l.add(t1); l.add(t2);

        when(tripRepository.findByUser_Id(any())).thenReturn(l);

        List<Double> summary = summaryService.createTotalSummary(1L);

        Assert.assertEquals(4.0, summary.get(0), 0.00005);

        Assert.assertEquals(7.0, summary.get(2), 0.00005);

        Assert.assertEquals(5.0, summary.get(1), 0.00005);
    }
}
