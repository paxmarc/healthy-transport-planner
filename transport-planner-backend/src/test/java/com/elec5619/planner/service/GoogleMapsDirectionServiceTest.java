package com.elec5619.planner.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by kristy on 23/10/16.
 */
public class GoogleMapsDirectionServiceTest {

    private GoogleMapsDirectionService directionService;

    @Before
    public void setUp() throws Exception {
        directionService = new GoogleMapsDirectionService();
    }

    @Test
    public void testNullAddresses() {
        Assert.assertNull(directionService.sendDirectionRequest(null, ""));
        Assert.assertNull(directionService.sendDirectionRequest("", null));
    }

}
