package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.UserBadgeRepository;
import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.Badge;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.domain.model.UserBadge;
import com.elec5619.planner.util.checkers.BadgeChecker;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

/**
 * Created by eliemoreau on 23/10/2016.
 */

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserBadgeServiceTest {

    @Autowired
    TestEntityManager entityManager;

    @Mock
    BadgeChecker badgeChecker;

    @Mock
    UserRepository userRepository;

    @Mock
    UserBadgeRepository userBadgeRepository;

    @InjectMocks
    UserBadgeService userBadgeService;

    User u;
    Badge b;

    @Before
    public void setUp() {
        userBadgeService = new UserBadgeService();
        MockitoAnnotations.initMocks(this);

        this.u = new User("m@m.m", "password", "m");
        entityManager.persist(this.u);
        this.b = new Badge("Bronze Member", "Be a member for 1 month.");
        entityManager.persist(this.b);

        // Inject a result to be returned when userRepository.findOne() is called.
        when(userRepository.findOne(this.u.getId())).thenReturn(this.u);
    }

    @Test
    public void testGetBadgesByUserId() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -2);
        u.setJoinDate(c.getTime());

        // Inject a result to be returned when userBadgeRepository.findByUserId() is called.
        UserBadge ub = new UserBadge(this.u, this.b);
        when(userBadgeRepository.findByUserId(this.u.getId())).thenReturn(Arrays.asList(ub));

        // Inject a result to be returned when badgeChecker.checkNewBadges() is called.
        when(badgeChecker.checkNewBadges(this.u.getId(), new HashSet<>()))
                .thenReturn(Arrays.asList(this.b));

        // Run the method that we are testing.
        Iterable<Badge> allBadges = userBadgeService.getBadgesByUserId(this.u.getId());

        // Assert that the new badge has been found and returned.
        assertTrue(allBadges.iterator().hasNext());
        assertEquals(this.b, allBadges.iterator().next());
    }

    @Test
    public void testUpdateBadgesByUserIdWithNoNewBadge() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        u.setJoinDate(c.getTime());

        // Inject a result to be returned when badgeChecker.checkNewBadges() is called.
        when(badgeChecker.checkNewBadges(this.u.getId(), new HashSet<>()))
                .thenReturn(Arrays.asList());

        // Run the method that we are testing.
        userBadgeService.updateBadgesByUserId(this.u.getId());

        // Verify that there was no attempt to save.
        verify(userBadgeRepository, times(0)).save(any(UserBadge.class));
    }

    @Test
    public void testUpdateBadgesByUserIdWithOneNewBadge() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -2);
        u.setJoinDate(c.getTime());

        // Inject a result to be returned when badgeChecker.checkNewBadges() is called.
        when(badgeChecker.checkNewBadges(this.u.getId(), new HashSet<>()))
                .thenReturn(Arrays.asList(this.b));

        // Run the method that we are testing.
        userBadgeService.updateBadgesByUserId(this.u.getId());

        // Verify that there was one attempt to save.
        verify(userBadgeRepository, times(1)).save(any(UserBadge.class));
    }

}
