package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.TripRepository;
import com.elec5619.planner.domain.dao.UserBadgeRepository;
import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.Trip;
import com.elec5619.planner.domain.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

/**
 * Created by kristy on 22/10/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class TripServiceTest {
    @Mock
    private TripRepository mockedTripRepository;

    @Mock
    private UserRepository mockedUserRepository;

    @Mock
    private UserBadgeService userBadgeService;

    @InjectMocks
    private TripService tripService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddTrip() {
        Trip trip = new Trip((long)5, 5.0, 1.0, 0.0);
        User user = new User("mail@mail.com", "password", "Bob");
        user.setId((long)10);

        when(mockedUserRepository.findOne((long)5)).thenReturn(user);
        trip.setUserId((long)10);
        when(mockedTripRepository.save(trip)).thenReturn(trip);

        trip = tripService.addTrip(trip);

        Assert.assertEquals(10, (long)trip.getUserId());
    }
}
