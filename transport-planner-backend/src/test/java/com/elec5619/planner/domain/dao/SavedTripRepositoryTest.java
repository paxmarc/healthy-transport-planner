package com.elec5619.planner.domain.dao;

import com.elec5619.planner.domain.model.SavedTrip;
import com.elec5619.planner.domain.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rolo on 23/10/2016.
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class SavedTripRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    SavedTripRepository savedTripRepository;

    @Autowired
    UserRepository userRepository;

    private User testUser;

    @Before
    public void setUp() throws Exception {
        testUser = new User("test@example.com", "secret", "test user");
        entityManager.persist(testUser);
    }

    @After
    public void tearDown() throws Exception {
        entityManager.clear();
    }

    @Test
    public void testFindByUserWhenUserHasSavedTrips() throws Exception {
        final String DEST = "1";
        final String ORIG = "A";

        SavedTrip st1 = new SavedTrip(ORIG, DEST, testUser);

        entityManager.persist(st1);

        List<SavedTrip> savedTrips = savedTripRepository.findByUser(testUser);

        assertEquals(1, savedTrips.size());
        assertEquals(DEST, savedTrips.get(0).getDest());
        assertEquals(ORIG, savedTrips.get(0).getOrigin());
        assertEquals(testUser.getId(), savedTrips.get(0).getUser().getId());
    }

    @Test
    public void testFindByUserWhenUserHasNoSavedTrips() throws Exception {
        List<SavedTrip> savedTrips = savedTripRepository.findByUser(testUser);

        assertEquals(0, savedTrips.size());
    }



}