package com.elec5619.planner.domain.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rolo on 23/10/2016.
 */
public class SavedTripTest {
    final String ORIGIN = "1 Cleveland Street, University of Sydney, Camperdown, Sydney, NSW, 2008";
    final String DEST = "1234 January Drive, St. Catharines, ON, Canada, L2J4J4";

    private SavedTrip savedTrip;
    private User user;

    @Before
    public void setUp() throws Exception {
        user = new User("test@example.com", "secret", "test user");
        savedTrip = new SavedTrip(ORIGIN, DEST, user);
    }

    @Test
    public void testGetOrigin() throws Exception {
        assertEquals(ORIGIN, savedTrip.getOrigin());
    }

    @Test
    public void testGetDest() throws Exception {
        assertEquals(DEST, savedTrip.getDest());
    }

    @Test
    public void testGetUser() throws Exception {
        assertEquals(user, savedTrip.getUser());
    }

}