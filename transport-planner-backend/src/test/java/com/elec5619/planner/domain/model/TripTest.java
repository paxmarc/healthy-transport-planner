package com.elec5619.planner.domain.model;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

/**
 * Created by kristy on 22/10/16.
 */
public class TripTest {

    @Test
    public void testCreateTrip() {
        Double e = 0.001;
        Trip trip = new Trip((long)1, 3.0, 2.0, 4.0);

        assertEquals(1, (long)trip.getUserId());
        assertEquals(3.0, trip.getWalkingDistance(), e);
        assertEquals(2.0, trip.getTrainDistance(), e);
        assertEquals(4.0, trip.getBusDistance(), e);
    }

    @Test
    public void testSetters() {
        Double e = 0.001;
        Trip trip = new Trip((long)1, 3.0, 2.0, 4.0);
        java.sql.Date c = new java.sql.Date(new GregorianCalendar().getTimeInMillis());

        trip.setUserId((long)2);
        trip.setWalkingDistance(2.0);
        trip.setTrainDistance(1.0);
        trip.setBusDistance(1.0);
        trip.setDate(c);

        assertEquals(2, (long)trip.getUserId());
        assertEquals(2.0, trip.getWalkingDistance(), e);
        assertEquals(1.0, trip.getTrainDistance(), e);
        assertEquals(1.0, trip.getBusDistance(), e);
        assertEquals(c.getTime(), trip.getDate().getTime());
    }
}
