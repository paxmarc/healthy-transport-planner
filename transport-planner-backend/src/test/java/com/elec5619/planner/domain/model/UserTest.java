package com.elec5619.planner.domain.model;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by marcuspaxton on 21/10/16.
 */
public class UserTest {

    @Test
    public void testCreate() {
        User u = new User("m@m.m", "password", "Test User");
        assertEquals("m@m.m", u.getEmail());
        assertEquals("password", u.getPassword());
        assertEquals("Test User", u.getName());
    }
}
