package com.elec5619.planner.domain.dao;

import com.elec5619.planner.domain.model.Trip;
import com.elec5619.planner.domain.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.*;
import java.sql.Date;
import java.time.Instant;
import java.util.*;

/**
 * Created by marcuspaxton on 22/10/16.
 */

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class TripRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    TripRepository tripRepository;

    @Test
    public void testDateRangeFetch() {
        User u = new User("m@m.m", "password", "Test User");

        u = entityManager.persist(u);

        Long id = u.getId();

        Trip t1 = new Trip(id, 1.0, 1.0, 1.0);
        // 1/1/2016
        t1.setDate(new java.sql.Date(new GregorianCalendar(2016, 1, 1).getTimeInMillis()));

        Trip t2 = new Trip(id, 1.0, 1.0, 1.0);
        t2.setDate(new java.sql.Date(new GregorianCalendar(2016, 1, 5).getTimeInMillis()));
        t1 = entityManager.persist(t1);
        t2 = entityManager.persist(t2);


        List<Trip> l = tripRepository.findByDateAfter(
                new java.sql.Date(new GregorianCalendar(2016, 1, 2).getTimeInMillis()));

        Assert.assertEquals(1, l.size());


        List<Trip> l2 = tripRepository.findByDateAfter(
                new java.sql.Date(new GregorianCalendar(2015, 12, 31).getTimeInMillis()));

        Assert.assertEquals(2, l2.size());

        List<Trip> l3 = tripRepository.findByDateAfter(
                new java.sql.Date(new GregorianCalendar(2016, 10, 31).getTimeInMillis()));


        Assert.assertEquals(0, l3.size());

        entityManager.clear();
    }

    @Test
    public void findByUserIdTest() {
        User u = new User("m@m.m", "password", "Test User");
        User v = new User("m@m.n", "password", "Test User");
        u = entityManager.persist(u);
        v = entityManager.persist(v);

        Long id = u.getId();
        Long id2 = v.getId();

        Trip t1 = new Trip(id, 1.0, 1.0, 1.0);
        // 1/1/2016
        t1.setDate(new java.sql.Date(new GregorianCalendar(2016, 1, 1).getTimeInMillis()));
        t1.setUser(u);

        Trip t2 = new Trip(id, 1.0, 1.0, 1.0);
        t2.setDate(new java.sql.Date(new GregorianCalendar(2016, 1, 5).getTimeInMillis()));
        t2.setUser(u);
        t1 = entityManager.persist(t1);
        t2 = entityManager.persist(t2);

        List<Trip> l1 = tripRepository.findByUser_Id(id);

        Assert.assertEquals(2, l1.size());

        List<Trip> l2 = tripRepository.findByUser_Id(id2);

        Assert.assertEquals(0, l2.size());

        entityManager.clear();

    }

    @Test
    public void combinedQueryTest() {
        User u = new User("m@m.m", "password", "Test User");
        User v = new User("m@m.n", "password", "Test User");
        u = entityManager.persist(u);
        v = entityManager.persist(v);

        Long id = u.getId();
        Long id2 = v.getId();

        Trip t1 = new Trip(id, 1.0, 1.0, 1.0);
        // 1/1/2016
        t1.setDate(new java.sql.Date(new GregorianCalendar(2016, 1, 1).getTimeInMillis()));
        t1.setUser(u);

        Trip t2 = new Trip(id, 1.0, 1.0, 1.0);
        t2.setDate(new java.sql.Date(new GregorianCalendar(2016, 1, 5).getTimeInMillis()));
        t2.setUser(u);
        t1 = entityManager.persist(t1);
        t2 = entityManager.persist(t2);

        List<Trip> l = tripRepository.findByUser_IdAndDateAfter(id,
                new Date(new GregorianCalendar(2016, 1, 2).getTimeInMillis()));

        Assert.assertEquals(1, l.size());

        entityManager.clear();
    }


}
