package com.elec5619.planner.domain.dao;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import com.elec5619.planner.domain.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Created by marcuspaxton on 21/10/16.
 */

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    UserRepository userRepository;

    @Test
    public void testFindByEmail() {
        entityManager.persist(new User("m@m.m", "password", "Test"));

        List<User> user = userRepository.findByEmail("m@m.m");

        assertEquals("m@m.m", user.get(0).getEmail());
    }
}
