package com.elec5619.planner.domain.model;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by eliemoreau on 21/10/16.
 */
public class BadgeTest {

    @Test
    public void testCreate() {
        Badge b = new Badge("Leeeeeeeeeeeeeroy!", "Kill 50 rookery whelps within 15 seconds.");
        assertEquals("Leeeeeeeeeeeeeroy!", b.getName());
        assertEquals("Kill 50 rookery whelps within 15 seconds.", b.getDescription());
    }

    @Test
    public void testSetters() {
        Badge b = new Badge("Leeeeeeeeeeeeeroy!", "Kill 50 rookery whelps within 15 seconds.");
        b.setName("Ambassador of the Alliance");
        b.setDescription("Earn Exalted reputation with all six Alliance factions.");

        assertEquals("Ambassador of the Alliance", b.getName());
        assertEquals("Earn Exalted reputation with all six Alliance factions.", b.getDescription());
    }
}
