package com.elec5619.planner.domain.model;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by eliemoreau on 22/10/2016.
 */
public class UserBadgeTest {

    User u;
    Badge b;

    @Before
    public void setUp() {
        this.u = new User("m@m.m", "password", "m");
        this.b = new Badge("Badge Name", "Badge Description");
    }

    @Test
    public void testCreate() {
        UserBadge ub = new UserBadge(this.u, this.b);
        assertEquals(this.u.getId(), ub.getUserId());
        assertEquals(this.u.getId(), ub.getUser().getId());
        assertEquals(this.b.getId(), ub.getBadge().getId());
    }

    @Test
    public void testSetters() {
        UserBadge ub = new UserBadge(this.u, this.b);

        ub.setUserId((long) 2);
        assertEquals(2, (long) ub.getUserId());

        User u2 = new User("n@n.n", "password2", "n");
        ub.setUser(u2);
        assertEquals(u2.getId(), ub.getUser().getId());

        Badge b2 = new Badge("Badge Name v2", "Badge Description v2");
        ub.setBadge(b2);
        assertEquals(b2.getId(), ub.getBadge().getId());

        java.sql.Date earnedDate = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
        ub.setEarnedDate(earnedDate);
        assertEquals(earnedDate, ub.getEarnedDate());
    }
}
