package com.elec5619.planner.domain.dao;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import com.elec5619.planner.domain.model.UserBadge;
import com.elec5619.planner.domain.model.Badge;
import com.elec5619.planner.domain.model.User;

/**
 * Created by eliemoreau on 22/10/16.
 */

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserBadgeRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    UserBadgeRepository userBadgeRepository;

    @Test
    public void testFindByUserId() {
        User user = new User("m@m.m", "password", "Test");
        entityManager.persist(user);
        Badge badge = new Badge("Badge Name", "Badge Description");
        entityManager.persist(badge);
        UserBadge userBadge = new UserBadge(user, badge);
        entityManager.persist(userBadge);

        Iterable<UserBadge> userBadges = userBadgeRepository.findByUserId(user.getId());

        UserBadge ubResult = userBadges.iterator().next();
        assertEquals(user.getId(), ubResult.getUser().getId());
        assertEquals(badge.getId(), ubResult.getBadge().getId());
    }
}
