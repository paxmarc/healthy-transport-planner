package com.elec5619.planner.util.wrappers;

/**
 * Created by marcuspaxton on 5/10/2016.
 *
 * Wrapper object for login requests. Will be populated with the request's email and password.
 */

public class LoginRequest {

    private String email;
    private String password;

    protected LoginRequest() {}

    public LoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format("[LoginRequest: email=%s, password=%s]", this.email, this.password);
    }

}
