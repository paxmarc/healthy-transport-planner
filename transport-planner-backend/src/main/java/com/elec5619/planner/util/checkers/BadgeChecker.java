package com.elec5619.planner.util.checkers;

import com.elec5619.planner.domain.dao.BadgeRepository;
import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.domain.model.Badge;
import com.elec5619.planner.service.JsonWebTokenService;
import com.elec5619.planner.service.SummaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.Date;

/**
 * Created by eliemoreau on 23/10/2016.
 *
 * BadgeChecker Util for checking whether a user has earned a badge.
 */

@Component
public class BadgeChecker {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BadgeRepository badgeRepository;

    @Autowired
    private SummaryService summaryService;

    /**
     * Checks whether a user meets the requirements for any Badge they haven't earned yet.
     * @param userId - The ID number of the desired user
     * @param currentBadges - Set of all badges that the user has already earned
     * @return A List of all Badges that should be newly assigned to a user
     */
    public List<Badge> checkNewBadges(Long userId, Set<Long> currentBadges) {
        List<Badge> newBadges = new ArrayList<>();
        Iterable<Badge> allBadges = badgeRepository.findAll();
        for (Badge badge : allBadges) {
            if (!currentBadges.contains(badge.getId()) && checkBadge(userId, badge)) {
                newBadges.add(badge);
            }
        }
        return newBadges;
    }

    /**
     * Checks whether a user meets the requirements for the given Badge.
     * @param userId - The ID number of the desired user
     * @param badge - The Badge that we are currently checking if the user has earned it
     * @return A boolean set to true if the user should earn the badge, or false otherwise
     */
    public boolean checkBadge(Long userId, Badge badge) {
        User user = userRepository.findOne(userId);

        // Get the total and weekly summaries.
        List<Double> totals = summaryService.createTotalSummary(userId);
        List<Double> weekly = summaryService.createWeeklySummary(userId);

        String name = badge.getName();
        Calendar c;

        switch(name) {
            // Walking Distance badges.
            case "Downtown Stroll":
                return checkThreshold(totals.get(0), 500.0);
            case "Afternoon Hike":
                return checkThreshold(totals.get(0), 5000.0);
            case "Marathon Walk":
                return checkThreshold(totals.get(0), 50000.0);

            // Bus Distance badges.
            case "Downtown Bus":
                return checkThreshold(totals.get(1), 500.0);
            case "Suburban Bus":
                return checkThreshold(totals.get(1), 5000.0);
            case "Intercity Bus":
                return checkThreshold(totals.get(1), 50000.0);

            // Train Distance badges.
            case "Downtown Train":
                return checkThreshold(totals.get(2), 500.0);
            case "Suburban Train":
                return checkThreshold(totals.get(2), 5000.0);
            case "Intercity Train":
                return checkThreshold(totals.get(2), 50000.0);

            // Membership badges.
            case "Bronze Member":
                c = createCalendar(Calendar.MONTH, -1);
                return checkThreshold(c.getTime(), user.getJoinDate());
            case "Silver Member":
                c = createCalendar(Calendar.MONTH, -6);
                return checkThreshold(c.getTime(), user.getJoinDate());
            case "Gold Member":
                c = createCalendar(Calendar.YEAR, -1);
                return checkThreshold(c.getTime(), user.getJoinDate());

            // Total Trips badges.
            case "Curious Adventurer":
                return checkThreshold(totals.get(3), 10.0);
            case "Seasoned Traveller":
                return checkThreshold(totals.get(3), 100.0);
            case "Grand Explorer":
                return checkThreshold(totals.get(3), 1000.0);

            // Week Trips badges.
            case "Occasional Adventurer":
                return checkThreshold(weekly.get(3), 5.0);
            case "Regular Traveller":
                return checkThreshold(weekly.get(3), 10.0);
            case "Enthusiastic Explorer":
                return checkThreshold(weekly.get(3), 15.0);

            // Unknown badge.
            default:
                return false;
        }
    }

    /**
     * Create a Calendar set to (current Date + (difference * timePeriod)).
     * @param timePeriod - The time duration to multiply by
     * @param difference - The amount of times to multiply the timePeriod by
     * @return Calendar object that reflects the time specified by the above equation
     */
    public Calendar createCalendar(int timePeriod, int difference) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(timePeriod, difference);
        return calendar;
    }

    /**
     * Checks whether the threshold requirement has been met.
     * @param current - The value that should be greater or equal to the threshold
     * @param threshold - The value that current should beat or be equal to
     * @return - A boolean set to true if current >= threshold, or false otherwise
     */
    public <T extends Comparable<T>> boolean checkThreshold(T current, T threshold) {
        return current.compareTo(threshold) >= 0;
    }
}
