package com.elec5619.planner.util.wrappers;

import java.util.List;
import java.util.Map;

/**
 * Created by kristy on 23/10/16.
 *
 * RouteDetails combines information for one route. A route contains an origin and destination. This information
 * contains the route routeName, arrival time, walking and transit distances, and directions to travel on the route.
 * Transit is defined as any sort of public transportation excluding walking.
 */
public class RouteDetails {

    private String origin;
    private String dest;
    private String arrivalTime;
    private String departureTime;

    // Must be shown to the user due to use of the Google Maps API.
    private String copyright;
    // Must be shown to the user due to use of the Google Maps API.
    private String[] routeWarnings;

    // Contains the mode of each part of the trip.
    private List<String> transitModes;
    // Contains the name of each part of the trip.
    private List<String> stepName;
    // Contains the departure time of each transit option.
    private List<String> transitDepartureTimes;
    private List<Integer> transitNumStops;

    // Total distance for a trip.
    private Double walkingDistance;
    private Double trainDistance;
    private Double busDistance;

    // Contains turn by turn directions about each part of a route.
    private Map<Integer, List<String>> tripDirections;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String[] getRouteWarnings() {
        return routeWarnings;
    }

    public void setRouteWarnings(String[] routeWarnings) {
        this.routeWarnings = routeWarnings;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Map<Integer, List<String>> getTripDirections() {
        return tripDirections;
    }

    public void setTripDirections(Map<Integer, List<String>> tripDirections) {
        this.tripDirections = tripDirections;
    }

    public List<String> getTransitModes() {
        return transitModes;
    }

    public void setTransitModes(List<String> transitModes) {
        this.transitModes = transitModes;
    }

    public List<String> getStepName() {
        return stepName;
    }

    public void setStepName(List<String> stepName) {
        this.stepName = stepName;
    }

    public List<String> getTransitDepartureTimes() {
        return transitDepartureTimes;
    }

    public void setTransitDepartureTimes(List<String> transitDepartureTimes) {
        this.transitDepartureTimes = transitDepartureTimes;
    }

    public List<Integer> getTransitNumStops() {
        return transitNumStops;
    }

    public void setTransitNumStops(List<Integer> transitNumStops) {
        this.transitNumStops = transitNumStops;
    }

    public Double getWalkingDistance() {
        return walkingDistance;
    }

    public void setWalkingDistance(Double walkingDistance) {
        this.walkingDistance = walkingDistance;
    }

    public Double getTrainDistance() {
        return trainDistance;
    }

    public void setTrainDistance(Double trainDistance) {
        this.trainDistance = trainDistance;
    }

    public Double getBusDistance() {
        return busDistance;
    }

    public void setBusDistance(Double busDistance) {
        this.busDistance = busDistance;
    }
}
