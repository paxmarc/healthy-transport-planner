package com.elec5619.planner.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 * Created by Rolo on 19/10/2016.
 *
 * Each SavedTrip represents a row in the saved_trip table.
 */
@Entity
public class SavedTrip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String origin;
    private String dest;

    @ManyToOne(targetEntity = User.class, optional = false)
    private User user;

    /* Hibernate requires this constructor. Do not delete. */
    protected SavedTrip() {}

    public SavedTrip(String origin, String dest, User user) {
        this.origin = origin;
        this.dest = dest;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDest() {
        return dest;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return String.format("SavedTrip[id=%s, origin='%s', dest='%s', user=%s]", id, origin, dest, user);
    }
}
