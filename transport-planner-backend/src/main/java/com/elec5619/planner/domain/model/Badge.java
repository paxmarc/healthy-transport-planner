package com.elec5619.planner.domain.model;

import javax.persistence.*;

/**
 * Created by eliemoreau on 20/10/2016.
 *
 * Badge represents a row in the badge table.
 */

@Entity
public class Badge {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;

    // Hibernate requires this constructor.
    protected Badge() {}

    public Badge(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("Badge[id=%s, name=%s, description=%s]", id, name, description);
    }
}
