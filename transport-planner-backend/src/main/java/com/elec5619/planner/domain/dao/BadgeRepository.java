package com.elec5619.planner.domain.dao;

import com.elec5619.planner.domain.model.Badge;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by eliemoreau on 22/10/2016.
 *
 * Database access object for the app Badges. Can add methods
 * here (read the Spring Data docs) to add custom queries to the
 * objects.
 */
public interface BadgeRepository extends CrudRepository<Badge, Long> {}
