package com.elec5619.planner.domain.dao;

import com.elec5619.planner.domain.model.SavedTrip;
import com.elec5619.planner.domain.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
/**
 * Created by Rolo on 22/10/2016.
 *
 * Data access object for saved_trip table.
 * Spring renders the function based on the name of the function and parameter.
 */
public interface SavedTripRepository extends CrudRepository<SavedTrip, Long> {

    /**
     * Retrieves all the saved trips from the database for a user.
     * @param user 
     * @return A list of saved trips filtered on the user id
     */
    List<SavedTrip> findByUser(User user);
}
