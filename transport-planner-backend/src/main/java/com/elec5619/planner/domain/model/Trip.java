package com.elec5619.planner.domain.model;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Trip represents a row in trip table.
 */

@Entity
@Table(name = "Trip")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double walkingDistance;
    private Double trainDistance;
    private Double busDistance;

    // Combines the 'userId' variable with the 'user' variable in the Trip table.
    @JoinColumn(name = "user_id")
    @ManyToOne(targetEntity = User.class, optional = false)
    private User user;

    // Differentiates the 'userId' variable from the 'user' variable by setting 'updatable' and 'insertable' to false.
    @Column(name = "user_id", updatable = false, insertable = false)
    private Long userId;

    private java.sql.Date date;

    protected Trip(){};

    public Trip(Long userId, Double walkingDistance, Double trainDistance, Double busDistance) {
        this.userId = userId;
        this.walkingDistance = walkingDistance;
        this.trainDistance = trainDistance;
        this.busDistance = busDistance;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Double getWalkingDistance() {
        return walkingDistance;
    }
    public void setWalkingDistance(Double walkingDistance) {
        this.walkingDistance = walkingDistance;
    }

    public Double getTrainDistance() {
        return trainDistance;
    }
    public void setTrainDistance(Double trainDistance) {
        this.trainDistance = trainDistance;
    }

    public Double getBusDistance() {
        return busDistance;
    }
    public void setBusDistance(Double busDistance) {
        this.busDistance = busDistance;
    }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public java.sql.Date getDate() {
        return date;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }


    @Override
    public String toString() {
        return String.format("Trip[userId=%s, walkingDistance=%s, trainDistance=%s, busDistance=%s, date=%s]",
                this.userId, this.walkingDistance, trainDistance, busDistance, date);
    }


}