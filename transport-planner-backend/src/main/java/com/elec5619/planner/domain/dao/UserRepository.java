package com.elec5619.planner.domain.dao;

import com.elec5619.planner.domain.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by marcuspaxton on 5/10/2016.
 *
 * Database access object for the app Users. Can add methods
 * here (read the Spring Data docs) to add custom queries to the
 * objects.
 */



public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * @param email - The email address of a user
     * @return A list of users with a matching email (should be only one).
     */
    List<User> findByEmail(String email);


}
