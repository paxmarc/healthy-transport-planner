package com.elec5619.planner.domain.dao;

import org.springframework.data.repository.CrudRepository;
import com.elec5619.planner.domain.model.UserBadge;

/**
 * Created by eliemoreau on 22/10/2016.
 *
 * Database access object for the app UserBadge. Can add methods
 * here (read the Spring Data docs) to add custom queries to the
 * objects.
 */

public interface UserBadgeRepository extends CrudRepository<UserBadge, Long> {

    /**
     * @param userId - The userId of a user.
     * @return An Iterable of UserBadges that the user has earned.
     */
    Iterable<UserBadge> findByUserId(Long userId);

}
