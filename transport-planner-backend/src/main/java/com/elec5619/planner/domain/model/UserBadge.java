package com.elec5619.planner.domain.model;

import javax.persistence.*;

/**
 * Created by eliemoreau on 22/10/2016.
 *
 * UserBadge represents a row in the user_badge table.
 * Stores each Badge that a User has earned.
 */

@Entity
public class UserBadge {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userBadgeId;

    @ManyToOne(targetEntity = User.class, optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "user_id", updatable = false, insertable = false)
    private Long userId;

    @ManyToOne(targetEntity = Badge.class, optional = false)
    @JoinColumn(name = "badge_id")
    private Badge badge;

    // Date that the User earned the Badge.
    private java.sql.Date earnedDate;

    // Hibernate requires this constructor.
    protected UserBadge() {}

    public UserBadge(User user, Badge badge) {
        this.user = user;
        this.userId = user.getId();
        this.badge = badge;
    }

    public Long getUserBadgeId() {
        return userBadgeId;
    }
    public void setUserBadgeId(Long userBadgeId) {
        this.userBadgeId = userBadgeId;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Badge getBadge() {
        return badge;
    }
    public void setBadge(Badge badge) {
        this.badge = badge;
    }

    public java.sql.Date getEarnedDate() {
        return earnedDate;
    }
    public void setEarnedDate(java.sql.Date earnedDate) {
        this.earnedDate = earnedDate;
    }

    @Override
    public String toString() {
        return String.format("UserBadge[user=%s, userId=%s, badge=%s, earnedDate=%s]", user, userId, badge, earnedDate);
    }
}
