package com.elec5619.planner.domain.dao;

import com.elec5619.planner.domain.model.Trip;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.TemporalType;
import java.util.Calendar;
import java.util.List;

/*
 * TripRepository is a database read object that allows access to the trip table.
 */

public interface TripRepository extends CrudRepository<Trip, Long> {

    List<Trip> findByDateAfter(java.sql.Date date);

    List<Trip> findByUser_Id(Long userId);

    List<Trip> findByUser_IdAndDateAfter(Long userId, java.sql.Date date);

}
