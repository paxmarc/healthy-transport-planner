package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.TripRepository;
import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.Trip;
import com.elec5619.planner.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * TripService allows the user's trip distance to be stored in the database.
 */
@Service
public class TripService {

    @Autowired
    private UserBadgeService userBadgeService;

    private TripRepository tripRepository;
    private UserRepository userRepository;

    @Autowired
    public TripService(TripRepository tripRepository, UserRepository userRepository) {
        Assert.notNull(tripRepository);
        Assert.notNull(userRepository);
        this.tripRepository = tripRepository;
        this.userRepository = userRepository;
    }

    public Trip addTrip(Trip newTrip) {
        User user = userRepository.findOne(newTrip.getUserId());
        newTrip.setUser(user);
        newTrip.setDate(new java.sql.Date(new GregorianCalendar().getTimeInMillis()));
        Trip savedTrip = tripRepository.save(newTrip);

        userBadgeService.updateBadgesByUserId(newTrip.getUserId());

        return savedTrip;
    }
}
