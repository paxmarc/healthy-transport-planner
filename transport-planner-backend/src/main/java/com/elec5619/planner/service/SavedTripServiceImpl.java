package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.SavedTripRepository;
import com.elec5619.planner.domain.model.SavedTrip;
import com.elec5619.planner.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rolo on 22/10/2016.
 *
 * A basic implementation of the SavedTripService.
 */
@Service
public class SavedTripServiceImpl implements SavedTripService {

    @Autowired
    private SavedTripRepository savedTripRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private JsonWebTokenService jsonWebTokenService;

    @Autowired
    private GoogleMapsDirectionService googleMapsDirectionService;

    @Override
    public List<SavedTrip> getSavedTrips(String auth) {
        String jwt = jsonWebTokenService.extractJwtHeaderToken(auth);
        String id = jsonWebTokenService.getSubject(jwt);
        User user = userService.getUserById(Long.parseLong(id));

        return getSavedTrips(user);
    }

    @Override
    public List<SavedTrip> getSavedTrips(User user) {
        if (user != null) {
            return savedTripRepository.findByUser(user);
        }

        return Collections.EMPTY_LIST;
    }

    @Override
    public boolean saveTrip(String auth, String origin, String destination) {
        String jwt = jsonWebTokenService.extractJwtHeaderToken(auth);
        String id = jsonWebTokenService.getSubject(jwt);
        User user = userService.getUserById(Long.parseLong(id));

        return saveTrip(user, origin, destination);
    }

    @Override
    public boolean saveTrip(User user, String origin, String destination) {
        if (user != null && isValidTrip(origin, destination)) {
            savedTripRepository.save(new SavedTrip(origin, destination, user));
            return true;
        }

        return false;
    }

    @Override
    public boolean deleteTrip(@NotNull User user, @NotNull Long id) {
        SavedTrip deletableTrip = savedTripRepository.findOne(id);

        if (deletableTrip != null && deletableTrip.getUser().getId() == user.getId()) {
            savedTripRepository.delete(id);
            return true;
        }

        return false;
    }


    private boolean isValidTrip(String origin, String destination) {
        return googleMapsDirectionService.isValidAddress(origin) &&
                googleMapsDirectionService.isValidAddress(destination);
    }
}
