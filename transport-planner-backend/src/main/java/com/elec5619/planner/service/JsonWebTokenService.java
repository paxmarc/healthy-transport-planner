package com.elec5619.planner.service;

import com.elec5619.planner.domain.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * Created by Rolo on 23/10/2016.
 */
@Service
public class JsonWebTokenService {

    public final String HEADER_PREFIX = "Bearer ";

    @Autowired
    private Environment environment;

    @Autowired
    private UserService userService;

    /**
     * The auth header has "Bearer JWT-token". This helper function extracts the JWT.
     * @param header - auth header
     * @return jwt token otherwise null
     */
    public String extractJwtHeaderToken(String header) {
        if (header.isEmpty() || header.length() < HEADER_PREFIX.length())
            return null;

        return header.substring(HEADER_PREFIX.length());
    }

    /**
     * Helper function for extracting the user id from the jwt token.
     * @param jwt - jwt token with subject as user id
     * @return the user id or throw an exception if the token has been tampered with
     */
    public String getSubject(String jwt) {
        return Jwts.parser()
                .setSigningKey(getSigningKey())
                .parseClaimsJws(jwt)
                .getBody()
                .getSubject();
    }


    /** Generates a JWT based on the provided user.
     * @param user - User to create the token for.
     * @return A string of the JWT, containing their user id, name and join date
     * (as a timestamp).
     */
    public String generateJwt(User user) {
        String key = environment.getProperty("auth.jwtkey");
        System.out.println(key);
        return Jwts.builder()
                .setSubject((user.getId()).toString())
                .claim("name", user.getName())
                .claim("joined", user.getJoinDate())
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();

    }

    /**
     * @return the signing key used by the jwt token
     */
    private String getSigningKey() {
        return environment.getProperty("auth.jwtkey");
    }
}
