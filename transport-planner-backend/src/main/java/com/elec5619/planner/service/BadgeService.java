package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.BadgeRepository;
import com.elec5619.planner.domain.model.Badge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by eliemoreau on 22/10/2016.
 *
 * BadgeService manages Badge objects.
 */

@Service
public class BadgeService {

    @Autowired
    private BadgeRepository badgeRepository;

    /**
     * Returns a list of all badges.
     * @return An Iterable of badge objects
     */
    public Iterable<Badge> getAllBadges() {
        return badgeRepository.findAll();
    }

    /**
     * Returns a badge given their badge ID.
     * @param badgeId - The ID number of the desired badge
     * @return That badge's badge object
     */
    public Badge getBadgeById(Long badgeId) {
        return badgeRepository.findOne(badgeId);
    }
}
