package com.elec5619.planner.service;

import com.elec5619.planner.util.wrappers.RouteDetails;

import java.util.List;

/**
 * Created by kristy on 22/10/2016.
 *
 * DirectionService provides information for the TripChoices and the TripDetails pages.
 */
public interface DirectionService {

    /**
     * Contains all information about all routes such as the route names, arrival times, directions etc. See the
     * RouteDetails class for more information.
     */
    List<RouteDetails> getAllRouteInformation();
}