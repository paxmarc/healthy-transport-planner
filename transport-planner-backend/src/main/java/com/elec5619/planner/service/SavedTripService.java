package com.elec5619.planner.service;

import com.elec5619.planner.domain.model.SavedTrip;
import com.elec5619.planner.domain.model.User;

import java.util.List;

/**
 * Created by Rolo on 23/10/2016.
 */
public interface SavedTripService {

    /**
     * Retrieve all the saved trip for the user (defined as the subject in the
     * jwt token).
     * @param auth - auth header with jwt token containing the subject as the user id
     * @return a list of saved trips or empty list if user can't be found
     */
    List<SavedTrip> getSavedTrips(String auth);

    /**
     * Retrieve all the saved trip for the user.
     * @param user
     * @return a list of saved trips or empty list if user can't be found
     */
    List<SavedTrip> getSavedTrips(User user);

    /**
     * Verify and save a trip for the user (defined as the subject in the jwt token).
     * @param auth - auth header with jwt token containing the subject as the user id
     * @param origin
     * @param destination
     * @return true if success save into the database, false otherwise
     */
    boolean saveTrip(String auth, String origin, String destination);

    /**
     * Verify and save a trip for the user.
     * @param user
     * @param origin
     * @param destination
     * @return true if success save into the database, false otherwise
     */
    boolean saveTrip(User user, String origin, String destination);

    /**
     * Checks and deletes a trip for the user.
     * @param user
     * @param id - trip id
     * @return true if deleted, false otherwise
     */
    boolean deleteTrip(User user, Long id);
}
