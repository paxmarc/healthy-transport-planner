package com.elec5619.planner.service;

import com.elec5619.planner.util.wrappers.RouteDetails;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;

import com.google.maps.*;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by kristy on 21/10/2016.
 *
 * GoogleMapsDirectionService calls the Google Maps Direction API to provide a list of available routes and directions for a
 * specific route.
*/
@Service
public class GoogleMapsDirectionService {

    @Autowired
    private GoogleParserService parser;

    private final GeoApiContext context;

    public GoogleMapsDirectionService() {
        this.context = new GeoApiContext().setApiKey("AIzaSyAIQYl4jN8Rt35MO-BkrS6R5jBMizCK6tY");
    }

    /**
     * Sends a new Google Maps Direction API request when called. The result of the request is given to the
     * GoogleParserService.
     * @return true if successful request, else false.
     */
    public List<RouteDetails> sendDirectionRequest(String origin, String dest) {
        if (origin == null || dest == null) {
            return null;
        }

        DirectionsApiRequest request = DirectionsApi.newRequest(this.context)
                .mode(TravelMode.TRANSIT)
                .alternatives(true)
                .units(Unit.METRIC)
                .origin(origin)
                .destination(dest);

        try {
            DirectionsResult googleMapsData = request.await();
            List<RouteDetails> routeDetails = parser.convertDirectionResult(googleMapsData);

            Collections.sort(routeDetails, new Comparator<RouteDetails>() {
                @Override
                public int compare(RouteDetails r1, RouteDetails r2) {
                    return Double.compare(r2.getWalkingDistance(), r1.getWalkingDistance());
                }
            });

            return routeDetails;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Checks if an address is valid by asking Google for the geocoding of the address.
     * @param address
     * @return true, if there is at geocode with the address.
     */
    public boolean isValidAddress(String address) {
        GeocodingApiRequest request = GeocodingApi.geocode(this.context, address);

        try {
            GeocodingResult[] response = request.await();
            return response.length > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
