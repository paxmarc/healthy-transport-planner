package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.UserBadgeRepository;
import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.Badge;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.domain.model.UserBadge;
import com.elec5619.planner.util.checkers.BadgeChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by eliemoreau on 22/10/2016.
 *
 * UserBadgeService manages UserBadge objects.
 */

@Service
public class UserBadgeService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserBadgeRepository userBadgeRepository;

    @Autowired
    private BadgeChecker badgeChecker;

    /**
     * Returns a list of user's badges, using the given user id.
     * @param userId - The ID number of the desired user
     * @return An Iterable of badge objects earned by the user
     */
    public Iterable<Badge> getBadgesByUserId(Long userId) {
        updateBadgesByUserId(userId);

        List<Badge> badges = new ArrayList<>();
        Iterable<UserBadge> userBadges = userBadgeRepository.findByUserId(userId);
        if (userBadges != null) {
            userBadges.forEach(userBadge -> badges.add(userBadge.getBadge()));
        }
        return badges;
    }

    /**
     * Checks whether the user has earned any new badges and adds them to UserBadge model.
     * @param userId - The ID number of the desired user
     * @return An Iterable of badge objects newly earned by the user
     */
    public void updateBadgesByUserId(Long userId) {
        Iterable<UserBadge> currentBadges = userBadgeRepository.findByUserId(userId);

        Set<Long> currentBadgeIds = new HashSet<>();
        if (currentBadges != null) {
            currentBadges.forEach(userBadge -> currentBadgeIds.add(userBadge.getBadge().getId()));
        }
        List<Badge> newBadges = badgeChecker.checkNewBadges(userId, currentBadgeIds);

        User user = userRepository.findOne(userId);
        for (Badge newBadge : newBadges) {
            UserBadge ub = new UserBadge(user, newBadge);
            ub.setEarnedDate(new java.sql.Date(new GregorianCalendar().getTimeInMillis()));
            userBadgeRepository.save(ub);
        }
    }

    /**
     * Return n Iterable of all UserBadges, containing Dates that the user has earned each badge on.
     * @param userId - The ID number of the desired user
     * @return An Iterable of UserBadge objects belonging to the desired user
     */
    public Iterable<UserBadge> getEarnedDatesByUserId(Long userId) {
        return userBadgeRepository.findByUserId(userId);
    }
}
