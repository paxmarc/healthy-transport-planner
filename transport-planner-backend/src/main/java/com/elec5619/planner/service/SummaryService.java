package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.TripRepository;
import com.elec5619.planner.domain.model.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by marcuspaxton on 24/10/16.
 *
 * Service for generating total statistics on all the trips a user has taken.
 * Covers the past week, as well as all-time.
 */

@Service
public class SummaryService {

    @Autowired
    private TripRepository tripRepository;

    /**
     * Calculate the weekly summaries of a user's trips.
     * @param userId - The ID number of the desired user
     * @return A List containing the walk, bus, and train distances, as well as the number of trips
     */
    public List<Double> createWeeklySummary(Long userId) {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -7);

        List<Trip> tripsFromPastWeek = tripRepository.findByUser_IdAndDateAfter(userId,
                new Date(calendar.getTimeInMillis()));

       return getTotals(tripsFromPastWeek);
    }

    /**
     * Calculate the total summaries of a user's trips.
     * @param userId - The ID number of the desired user
     * @return A List containing the walk, bus, and train distances, as well as the number of trips
     */
    public List<Double> createTotalSummary(Long userId) {

        List<Trip> trips = tripRepository.findByUser_Id(userId);

        return getTotals(trips);
    }

    /**
     * Calculate summaries from the given trips.
     * @param trips - A List of trips to create summaries out of
     * @return A List containing the walk, bus, and train distances, as well as the number of trips
     */
    private List<Double> getTotals(List<Trip> trips) {
        Double walk = 0.0, bus = 0.0, train = 0.0;
        List<Double> totals = new ArrayList<>();
        for (Trip t: trips) {
            walk += t.getWalkingDistance();
            bus += t.getBusDistance();
            train += t.getTrainDistance();
        }

        totals.add(walk);
        totals.add(bus);
        totals.add(train);
        totals.add((double) trips.size());

        return totals;
    }
}
