package com.elec5619.planner.service;


import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

/**
 * Created by marcuspaxton on 4/10/2016.
 *
 * Service for managing User objects.
 */

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        Assert.notNull(userRepository);
        this.userRepository = userRepository;
    }

    /**
     * Returns a list of all users.
     * @return An Iterable of user objects.
     */
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Returns a user given their user ID.
     * @param userId - The ID number of the desired user.
     * @return That user's user object.
     */
    public User getUserById(Long userId) {
        return userRepository.findOne(userId);
    }

    /**
     * Add a new user to the database.
     * @param newUser - The user object submitted from the application.
     * @return The saved user object if user does not already exist, otherwise
     * null.
     */
    public User addUser(User newUser) {
        if (doesUserExist(newUser.getEmail())) return null;
        newUser.setJoinDate(new Date());
        User savedUser = userRepository.save(newUser);
        return savedUser;
    }

    /**
     * Checks if a user already exists in the database.
     * @param email
     * @return
     */
    private boolean doesUserExist(String email) {
        List<User> foundUsers = userRepository.findByEmail(email);
        return !foundUsers.isEmpty();
    }

}
