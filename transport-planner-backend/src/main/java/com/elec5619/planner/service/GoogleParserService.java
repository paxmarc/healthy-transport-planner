package com.elec5619.planner.service;

import com.elec5619.planner.util.wrappers.RouteDetails;
import com.google.maps.model.*;
import org.jsoup.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeFieldType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

import static java.lang.Math.toIntExact;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kristy on 23/10/16.
 *
 * GoogleParserService parses through the result of the Google Maps Direction API request to reduce the amount of
 * information given to the frontend.
 */
@Service
public class GoogleParserService implements DirectionService {

    private List<RouteDetails> allRouteInfo;
    private DateTimeFormatter fmt;

    public static final String SUBWAY = "subway";
    public static final String BUS = "bus";
    public static final String WALK = "walk";

    @Override
    public List<RouteDetails> getAllRouteInformation() {
        return this.allRouteInfo;
    }

    /**
     * Converts the given DirectionResult to a subset of usable information.
     * @param directionsResult - The result of a Google Maps Directions API call.
     * @return A list of RouteDetails.
     */
    public List<RouteDetails> convertDirectionResult(DirectionsResult directionsResult) {
        allRouteInfo = new ArrayList<>();
        for (DirectionsRoute route : directionsResult.routes) {
            RouteDetails routeDetails = new RouteDetails();

            routeDetails.setCopyright(route.copyrights);
            routeDetails.setRouteWarnings(route.warnings);

            for (DirectionsLeg leg : route.legs) {

                routeDetails.setOrigin(leg.startAddress);
                routeDetails.setDest(leg.endAddress);

                fmt = DateTimeFormat.forPattern("HH:mm");

                DateTime currDateTime = new DateTime();
                routeDetails.setDepartureTime(fmt.print(leg.departureTime));

                DateTimeComparator comparator = DateTimeComparator.getInstance(DateTimeFieldType.minuteOfHour());

                // If the step contains only walking, no time will be set.
                if (comparator.compare(leg.arrivalTime, null) == 0) {
                    DateTime arrivalTime = currDateTime.plusSeconds(toIntExact(leg.duration.inSeconds));

                    routeDetails.setArrivalTime(fmt.print(arrivalTime));
                }  else {
                    routeDetails.setArrivalTime(fmt.print(leg.arrivalTime));
                }

                convertDirectionLeg(leg, routeDetails);
            }
            this.allRouteInfo.add(routeDetails);
        }

        return allRouteInfo;
    }

    /**
     * Converts a DirectionLeg into usable information about a specific trip.
     * @param leg - A DirectionLeg which a route with one origin and destination.
     */
    private void convertDirectionLeg(DirectionsLeg leg, RouteDetails routeDetails) {
        List<String> stepModes = new ArrayList<>();
        List<String> stepNames = new ArrayList<>();
        List<String> departureTimes = new ArrayList<>();

        Double walkingDistance = 0.0;
        Double trainDistance = 0.0;
        Double busDistance = 0.0;

        Map<Integer, List<String>> tripDirections = new LinkedHashMap<>();
        int index = 0;
        List<Integer> transitNumStops = new ArrayList<>();

        // Each step is a separate transit option.
        for(DirectionsStep step : leg.steps) {
            String mode = "";
            String name = "";
            if (step.travelMode.toString().equals("walking")) {
                walkingDistance += (double)step.distance.inMeters;
                mode = WALK;
                name = step.htmlInstructions;
                departureTimes.add(null);
                transitNumStops.add(null);

                List<String> turnDirections = new ArrayList<>();

                // Turn by turn directions.
                for (DirectionsStep substep : step.steps) {
                    if(substep.htmlInstructions != null) {
                        String plainInstructions = Jsoup.parse(substep.htmlInstructions).text();
                        turnDirections.add(plainInstructions);
                    }
                }

                tripDirections.put(index, turnDirections);
            } else {
                // If the step is a public transportation option.
                if (step.transitDetails != null) {
                    transitNumStops.add(step.transitDetails.numStops);

                    tripDirections.put(index, null);

                    if (step.transitDetails.line.vehicle.name.equals("Train")) {
                        trainDistance += (double)step.distance.inMeters;
                        mode = SUBWAY;
                        name = step.transitDetails.line.name;
                    } else if (step.transitDetails.line.vehicle.name.equals("Bus")) {
                        busDistance += (double)step.distance.inMeters;
                        mode = BUS;
                        name = step.transitDetails.line.shortName;
                    } else if (step.transitDetails.line.vehicle.name.equals("Tram")) {
                        mode = SUBWAY;
                        name = step.transitDetails.line.name;
                    }
                    departureTimes.add(fmt.print(step.transitDetails.departureTime));
                }
            }
            index++;
            stepModes.add(mode);
            stepNames.add(name);
        }

        routeDetails.setTransitModes(stepModes);
        routeDetails.setStepName(stepNames);
        routeDetails.setTransitDepartureTimes(departureTimes);
        routeDetails.setTripDirections(tripDirections);
        routeDetails.setTransitNumStops(transitNumStops);

        routeDetails.setWalkingDistance(walkingDistance);
        routeDetails.setTrainDistance(trainDistance);
        routeDetails.setBusDistance(busDistance);
    }
}
