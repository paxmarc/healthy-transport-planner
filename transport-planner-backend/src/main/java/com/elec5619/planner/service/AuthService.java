package com.elec5619.planner.service;

import com.elec5619.planner.domain.dao.UserRepository;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.util.wrappers.LoginRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by marcuspaxton on 4/10/2016.
 *
 * Service for authentication functions - password hashing
 * and verification, login, session token generation.
 */
@Service
public class AuthService {

    @Autowired
    private Environment env;

    @Autowired
    private JsonWebTokenService jwt;

    @Autowired
    private UserRepository userRepository;

    /**
     * Hashes an input password using BCrypt.
     * @param newUser - The user whose password is to be hashed.
     * @return The hashed password.
     */
    public User hashUserPassword(User newUser) {
        newUser.setPassword(BCrypt.hashpw(newUser.getPassword(), BCrypt.gensalt()));
        return newUser;
    }

    /**
     * Verifies an input password against the BCrypt hash.
     * @param password - The plaintext password entered by the user.
     * @param hashed - The hashed password from the database.
     * @return The boolean result of the verification.
     */
    public boolean verifyPassword(String password, String hashed) {
        return BCrypt.checkpw(password, hashed);
    }

    /**
     * Attempts to login the user using the provided username and password.
     * @param request - The login request containing the email and password.
     * @return If the user exists and the login is valid, return a JWT with
     * their user ID as the subject. Otherwise, return null.
     */
    public String loginUser(LoginRequest request) {
        List<User> foundUsers = userRepository.findByEmail(request.getEmail());
        if (foundUsers.isEmpty()) return null;

        User toLogin = foundUsers.get(0);
        System.out.println(toLogin);

        // check if password matches
        if (verifyPassword(request.getPassword(), toLogin.getPassword())) {
            return jwt.generateJwt(toLogin);
        } else {
            return null;
        }
    }



}
