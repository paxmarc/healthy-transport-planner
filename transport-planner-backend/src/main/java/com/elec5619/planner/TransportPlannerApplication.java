package com.elec5619.planner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransportPlannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransportPlannerApplication.class, args);
	}
}
