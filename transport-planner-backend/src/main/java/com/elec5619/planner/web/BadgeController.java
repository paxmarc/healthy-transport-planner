package com.elec5619.planner.web;

import com.elec5619.planner.domain.model.Badge;
import com.elec5619.planner.service.BadgeService;
import com.elec5619.planner.service.JsonWebTokenService;
import com.elec5619.planner.service.UserBadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by eliemoreau on 22/10/2016.
 *
 * BadgeController is the REST endpoint for the Badge and UserBadge models.
 */

@RestController
@CrossOrigin(origins="http://localhost:8100")
@RequestMapping(path="/badge")
public class BadgeController {

    @Autowired
    private BadgeService badgeService;

    @Autowired
    private UserBadgeService userBadgeService;

    @Autowired
    private JsonWebTokenService jwtService;

    /**
     * Return a list of all badges.
     * @return An Iterable of all badges
     */
    @RequestMapping(method = GET)
    public ResponseEntity<HashMap<String, Object>> getAllBadges() {
        HashMap<String, Object> response = new HashMap<>();
        response.put("badges", badgeService.getAllBadges());

        return ResponseEntity.ok(response);
    }

    /**
     * Return a badge based on the badge id.
     * @param badgeId - The ID number of the desired badge
     * @return That badge's badge object
     */
    @RequestMapping(path="/{id}", method = GET)
    public Badge getBadgeById(@PathVariable Long badgeId) {
        return badgeService.getBadgeById(badgeId);
    }

    /**
     * Return a list of all badges that the user with the given id has earned.
     * @param auth - The authorised token of the desired user
     * @return The user's badges that they have earned
     */
    @RequestMapping(path="/user", method = GET)
    public ResponseEntity<HashMap<String, Object>> getBadgesByUserId(
            @RequestHeader(value="Authorization") String auth) {
        String jwt = jwtService.extractJwtHeaderToken(auth);
        Long userId = Long.parseLong(jwtService.getSubject(jwt));

        HashMap<String, Object> response = new HashMap<>();
        if (userId != null) {
            response.put("badges", userBadgeService.getBadgesByUserId(userId));
        }

        return ResponseEntity.ok(response);
    }

    /**
     * Return a list of all Dates that the user has earned each badge on.
     * @param auth - The authorised token of the desired user
     * @return The user's Dates on which the user earned each badge
     */
    @RequestMapping(path="/user/earned", method = GET)
    public ResponseEntity<HashMap<String, Object>> getEarnedDatesByUserId(
            @RequestHeader(value="Authorization") String auth) {
        String jwt = jwtService.extractJwtHeaderToken(auth);
        Long userId = Long.parseLong(jwtService.getSubject(jwt));

        HashMap<String, Object> response = new HashMap<>();
        if (userId != null) {
            response.put("earnedDates", userBadgeService.getEarnedDatesByUserId(userId));
        }

        return ResponseEntity.ok(response);
    }

}
