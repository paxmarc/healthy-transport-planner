package com.elec5619.planner.web;

import com.elec5619.planner.service.GoogleMapsDirectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@CrossOrigin(origins="http://localhost:8100")
@RequestMapping(path="/trip-details")
public class TripDetailsController {

    @Autowired
    private GoogleMapsDirectionService googleMapsService;

    /**
     * Sends a request with the given origin and destination addresses to the Google Maps Direction API.
     * The resulting data is reduced to a number of RouteDetail objects.
     * @param origin - The origin address.
     * @param dest - The destination address.
     * @return An object containing the parsed data of the result of the Google Directions request.
     */
    @RequestMapping(path="/", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllRouteInformation(
            @RequestParam String origin,
            @RequestParam String dest) {
        HashMap<String, Object> response = new HashMap<>();
        response.put("allRoutes", this.googleMapsService.sendDirectionRequest(origin, dest));
        return ResponseEntity.ok(response);
    }
}
