package com.elec5619.planner.web;

import com.elec5619.planner.service.JsonWebTokenService;
import com.elec5619.planner.service.SummaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Created by marcuspaxton on 24/10/16.
 */

@RestController
@CrossOrigin(origins="http://localhost:8100")
@RequestMapping(path="/stats")
public class SummaryController {

    @Autowired
    SummaryService summaryService;

    @Autowired
    private JsonWebTokenService jwtService;

    /**
     * Return both the weekly and total summaries of a user's trips.
     * @param auth - The auth token of the desired user
     * @return An object containing the weekly and total summaries.
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getWeeklyAndTotalSummary(
            @RequestHeader(value="Authorization") String auth) {
        String jwt = jwtService.extractJwtHeaderToken(auth);
        Long userId = Long.parseLong(jwtService.getSubject(jwt));

        HashMap<String, Object> response = new HashMap<>();
        response.put("weekly", summaryService.createWeeklySummary(userId));
        response.put("total", summaryService.createTotalSummary(userId));

        return ResponseEntity.ok(response);
    }

}
