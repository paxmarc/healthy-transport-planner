package com.elec5619.planner.web;

import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.service.AuthService;
import com.elec5619.planner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by marcuspaxton on 4/10/2016.
 */

@RestController
@RequestMapping(path="/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        Assert.notNull(userService);
        this.userService = userService;
    }

    /**
     * @return A list of all the users contained in the database.
     */
    @RequestMapping(method = GET)
    public Iterable<User> getAllUsers() {
        return userService.getAllUsers();
    }


    /**
     * @return A user object corresponding to the given ID.
     */
    @RequestMapping(path="/{id}", method = GET)
    public User getUserById(@PathVariable Long id)  {
        return userService.getUserById(id);
    }


}
