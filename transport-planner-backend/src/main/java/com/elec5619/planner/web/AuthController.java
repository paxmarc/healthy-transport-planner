package com.elec5619.planner.web;

import com.elec5619.planner.service.JsonWebTokenService;
import com.elec5619.planner.util.wrappers.LoginRequest;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.service.AuthService;
import com.elec5619.planner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.HashMap;

/**
 * Created by marcuspaxton on 5/10/2016.
 */

@RestController
@CrossOrigin(origins= "http://localhost:8100")
@RequestMapping(path="/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private UserService userService;

    @Autowired
    private JsonWebTokenService jsonWebTokenService;


    /**
     * Registers a user in the database.
     * @param newUser - The given request body, deserialised as a User object
     * @return A JWT string containing the added user object, with status code 200 if registration
     * is successful. Otherwise, return 401.
     */
    @RequestMapping(path="/signup", method = POST)
    public ResponseEntity<HashMap<String, Object>> registerUser(@RequestBody User newUser) {
        HashMap<String, Object> response = new HashMap<>();
        // Attempt to add the user to the database.
        newUser = authService.hashUserPassword(newUser);
        User savedUser = userService.addUser(newUser);

        if (savedUser != null) {
            // If a user object is returned, generate a token
            String jwt = jsonWebTokenService.generateJwt(savedUser);
            response.put("token", jwt);
            return ResponseEntity.ok(response);
        } else {
            // Otherwise, return 401.
            response.put("success", false);
            return ResponseEntity.status(401).body(response);
        }

    }

    /**
     * Logs a user in to the site.
     * @param request - The given request body, deserialised as a LoginRequest object
     * @return A JWT string containing the logged-in user, with status code 200 if login
     * is successful. Otherwise, return 401.
     */
    @RequestMapping(path="/login", method = POST)
    public ResponseEntity<HashMap<String, Object>> loginUser(@RequestBody LoginRequest request) {
        System.out.println("Login request received from " + request);
        String jwt = authService.loginUser(request);
        HashMap<String, Object> response = new HashMap<>();
        if (jwt != null) {
            response.put("success", true);
            response.put("token", jwt);
            return ResponseEntity.ok(response);
        } else {
            response.put("success", false);
            return ResponseEntity.status(401).body(response);
        }
    }
}
