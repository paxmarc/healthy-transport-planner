package com.elec5619.planner.web;

import com.elec5619.planner.domain.model.Trip;
import com.elec5619.planner.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.util.Assert;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * TripController is the REST endpoint for the trip table.
 *
 * Allows the adding of Trip objects to the database.
 */
@RestController
@CrossOrigin(origins= "http://localhost:8100")
@RequestMapping(path="/trip")
public class TripController {

    private TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        Assert.notNull(tripService);
        this.tripService = tripService;
    }

    /**
     * Adds a trip.
     * @param newTrip - Request body containing a new Trip.
     * @return The added Trip object.
     */
    @RequestMapping(path = "/new", method = POST)
    public Trip addTripByUserId(@RequestBody Trip newTrip) {
        return tripService.addTrip(newTrip);
    }
}