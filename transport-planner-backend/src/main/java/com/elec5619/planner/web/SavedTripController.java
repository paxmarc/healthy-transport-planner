package com.elec5619.planner.web;

import com.elec5619.planner.domain.model.SavedTrip;
import com.elec5619.planner.domain.model.User;
import com.elec5619.planner.service.JsonWebTokenService;
import com.elec5619.planner.service.SavedTripService;
import com.elec5619.planner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.logging.Logger;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by Rolo on 21/10/2016.
 *
 * SavedTrip REST endpoint.
 */
@RestController
@CrossOrigin(origins="http://localhost:8100")
@RequestMapping(path="/savedtrip")
public class SavedTripController {

    private final static Logger LOGGER = Logger.getLogger(SavedTripController.class.getName());

    @Autowired
    SavedTripService savedTripService;

    @Autowired
    UserService userService;

    @Autowired
    JsonWebTokenService jsonWebTokenService;

    /**
     * Returns all the savedtrip for a user (defined as the subject in the jwt).
     * Expects the jwt to be the Authorization header.
     * @param auth - auth header containing a jwt with the subject as the user id
     * @return
     */
    @RequestMapping(method = GET)
    @ResponseBody
    public ResponseEntity<HashMap<String, Object>> getAllSavedTrips(
            @RequestHeader(value="Authorization") String auth) {
        LOGGER.info("GET /savedtrip received: " + auth);
        HashMap<String, Object> response = new HashMap<>();
        response.put("savedtrip", savedTripService.getSavedTrips(auth));

        return ResponseEntity.ok(response);
    }

    /**
     * Saves a trip.
     * @param auth - auth header containing a jwt with the subject as the user id
     * @param savedTrip - object containing destination, origin, and user
     * @return json object with success = true if trip is saved, false otherwise
     */
    @RequestMapping(method = POST)
    @ResponseBody
    public ResponseEntity<HashMap<String, Object>> saveTrip(
            @RequestHeader(value="Authorization") String auth,
            @RequestBody SavedTrip savedTrip) {
        LOGGER.info("POST /savedtrip: [%s] [%s]".format(auth.substring(0, 10), savedTrip));

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", savedTripService.saveTrip(auth, savedTrip.getOrigin(), savedTrip.getDest()));

        return ResponseEntity.ok(response);
    }

    /**
     * Deletes a saved trip given the trip id.
     * It checks that the trip belongs to the user.
     * @param auth - auth header (used to authenticate user).
     * @param id - trip id
     * @return json object with success = true if trip is deleted, false otherwise
     */
    @RequestMapping(path="/{id}", method = DELETE)
    public ResponseEntity<HashMap<String, Object>> deleteTrip(
            @RequestHeader(value="Authorization") String auth,
            @PathVariable Long id) {
        LOGGER.info("DELETE /savedtrip: [%s] [%s]".format(auth, id.toString()));

        HashMap<String, Object> response = new HashMap<>();

        String jwt = jsonWebTokenService.extractJwtHeaderToken(auth);
        Long userId = Long.parseLong(jsonWebTokenService.getSubject(jwt));

        User user =  userService.getUserById(userId);
        response.put("success", savedTripService.deleteTrip(user, id));

        return ResponseEntity.ok(response);
    }
}
