setup:
	docker pull postgres:9.4

docker:
	docker run -e POSTGRES_DB=TransportPlanner -e POSTGRES_USER=root -e POSTGRES_PASSWORD=root -p 5432:5432 -d postgres:9.4

mvn:
	mvn clean install -DskipTests -f ./transport-planner-backend/pom.xml
	mvn spring-boot:run -f ./transport-planner-backend/pom.xml

sql:
	psql -h localhost -U root -p 5432 -d TransportPlanner

data:
	psql -h localhost -U root -p 5432 -f data.sql -d TransportPlanner

clean:
	docker stop $(shell docker ps -a -q)
	docker rm $(shell docker ps -a -q)

webpack:
	(cd app; node_modules/webpack/bin/webpack.js --progress --colors -d)

watch:
	(cd app; node_modules/webpack/bin/webpack.js --progress --colors -d --watch)

serve:
	(cd app; ionic serve)
